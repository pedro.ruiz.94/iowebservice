#ifndef WebServer_h
#define WebServer_h

#include <ESPAsyncWebServer.h>
#include <lang.h>
#include <API.h>

#define SET_DEVICE_STATE_ROOT "/set/deviceState"
#define GET_DEVICE_STATE_ROOT "/get/deviceState"
#define GET_DEVICES_ROOT "/get/devices"
#define DELIMITER "/"

String findExtra(String *extras);
void homeRequest(AsyncWebServerRequest *request);
void notFound(AsyncWebServerRequest *request);
void setDeviceState(AsyncWebServerRequest *request);
void getDeviceState(AsyncWebServerRequest *request);
void getDevices(AsyncWebServerRequest *request);
void initServer();
void sendPlainResponse(AsyncWebServerRequest *request, int code, String response);

#endif