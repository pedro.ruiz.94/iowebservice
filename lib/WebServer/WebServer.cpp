#include "WebServer.h"

AsyncWebServer server(8080);
// Devices definition: name, type, gpio
int nDevices = 2;
IODevice devices[] = {
    IODevice("door", INPUT, 14),
    IODevice("lock", OUTPUT, 15)};
API api(devices, nDevices);

String findExtra(String *extras)
{
    String token = "";

    if ((*extras).startsWith(DELIMITER))
        (*extras).remove(0, 1);

    int indexOff = (*extras).indexOf(DELIMITER);

    if (indexOff != -1)
    {
        token = (*extras).substring(0, indexOff);
        (*extras).remove(0, indexOff + 1);
    }
    else
    {
        token = *extras;
        *extras = "";
    }
    return token;
}

void homeRequest(AsyncWebServerRequest *request)
{
    Serial.println(REQUEST + request->url());
    Serial.println("homeRequest");
    sendPlainResponse(request, 200, WELCOME);
}

void notFound(AsyncWebServerRequest *request)
{
    Serial.println(REQUEST + request->url());
    Serial.println("notFound");
    sendPlainResponse(request, 404, NOT_FOUND);
}

void setDeviceState(AsyncWebServerRequest *request)
{
    Serial.println(REQUEST + request->url());
    Serial.println("setDeviceState");
    String extras = request->url();
    extras.replace(SET_DEVICE_STATE_ROOT, "");
    String deviceName = findExtra(&extras);
    String status = findExtra(&extras);
    if (deviceName == "" || status == "" || extras != "")
        sendPlainResponse(request, 400, BAD_REQUEST);
    String response = api.setDeviceState(deviceName, status);
    sendPlainResponse(request, 200, response);
}

void getDeviceState(AsyncWebServerRequest *request)
{
    Serial.println(REQUEST + request->url());
    Serial.println("getDeviceState");
    String extras = request->url();
    extras.replace(GET_DEVICE_STATE_ROOT, "");
    String deviceName = findExtra(&extras);
    if (deviceName == "" || extras != "")
        sendPlainResponse(request, 400, BAD_REQUEST);
    String response = api.getDeviceState(deviceName);
    sendPlainResponse(request, 200, response);
}

void getDevices(AsyncWebServerRequest *request)
{
    Serial.println(REQUEST + request->url());
    Serial.println("getDevices");
    sendPlainResponse(request, 501, "Not implemented");
}

void sendPlainResponse(AsyncWebServerRequest *request, int code, String response)
{
    request->send(code, "text/plain", response);
    Serial.println(response);
}

void initServer()
{
    api.initializeDevices();
    server.on("/", HTTP_GET, homeRequest);
    server.on(SET_DEVICE_STATE_ROOT, HTTP_PUT, setDeviceState);
    server.on(GET_DEVICE_STATE_ROOT, HTTP_GET, getDeviceState);
    server.on(GET_DEVICES_ROOT, HTTP_GET, getDeviceState);

    server.onNotFound(notFound);

    server.begin();
    Serial.println("HTTP server started");
}
