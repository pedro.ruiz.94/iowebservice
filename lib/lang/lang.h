#define LANG_ENG

#ifndef lang_h
#define lang_h
//English
#ifdef LANG_ENG
#define SERVICE_NAME "ESP"
#define S_ON "ON"
#define S_OFF "OFF"
#define DEVICE_NOT_FOUND "Device not found: "
#define ERROR "ERROR"
#define NON_OUTPUT_DEVICE_ERROR "Not possible to set value in non output device"
#define NON_BOOLEAN_VALUE "Not possible to set a value different than: ON/OFF"
#define OK_RESPONSE "Operation Completed"
#define WELCOME "Welcome to ESP service"
#define NOT_FOUND "Request not found"
#define REQUEST "Request: "
#define BAD_REQUEST "Bad request"
#endif
// Español
#ifdef LANG_ESP
#define ON "ON"
#define OFF "OFF"
#define DEVICE_NOT_FOUND "Dispositivo no encontrado: "
#define ERROR "ERROR"
#define NON_OUTPUT_DEVICE_ERROR "No es posible establecer el valor de un dispositivo que no es de salida"
#define NON_BOOLEAN_VALUE "No es posible establecer un estado diferente de ON/OFF"
#define OK_RESPONSE "Operacion completada"
// TODO...
#endif
#endif