#ifndef WifiUtils_h
#define Wifiutils_h

#include <Arduino.h>
#include <ESP8266WiFi.h>

void ConnectWiFi(String ssid, String password);
#endif