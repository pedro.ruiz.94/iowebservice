#include "DeviceUtils.h"

// NUll Object
IODevice::IODevice()
{
    this->name = "...";
    this->type = 999;
    this->gpio = 999;
}

IODevice::IODevice(String name, int type, int gpio)
{
    this->name = name;
    this->type = type;
    this->gpio = gpio;
}

String IODevice::getName()
{
    return this->name;
}

int IODevice::getType()
{
    return this->type;
}

int IODevice::getGPIO()
{
    return this->gpio;
}

bool IODevice::operator==(const IODevice other)
{
    return this->name == other.name && this->type == other.type && this->gpio == other.gpio;
}