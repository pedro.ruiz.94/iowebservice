#ifndef DeviceUtils_h
#define DeviceUtils_h

#include <Arduino.h>

class IODevice
{
    String name;
    int type;
    int gpio;

public:
    IODevice();
    IODevice(String name, int type, int gpio);
    String getName();
    int getType();
    int getGPIO();
    bool operator==(const IODevice other);
};

#endif