#include <API.h>
/** Contructor de la clase
 * 
 *  @param devices dispositivos en configuracion
 *  @param nDevices numero de dispositivos en configuracion
 **/
API::API(IODevice devices[], int nDevices)
{
    this->devices = devices;
    this->nDevices = nDevices;
}
/** Metodo para buscar dentro de los dispositivos configurados
 * 
 * @param device_name nombre del dispositivo
 * @return dispositivo buscado, null si no se encuentra
*/
IODevice *API::findDevice(String deviceName)
{
    for (int i = 0; i < nDevices; i++)
        if (devices[i].getName() == deviceName)
            return &devices[i];
    // IODevice nullDevice;
    return nullptr;
}
/** Metodo para inicializar los dispositivos como GPIODevices
 * 
 **/
void API::initializeDevices()
{
    for (int i = 0; i < nDevices; i++)
    {
        pinMode(devices[i].getGPIO(), devices[i].getType());
    }
}
/** Metodo para establecer el estado en OutputDevices
 * 
 * @param deviceName nombre del dispositivo
 * @param state estado que se desea establecer
 * @return respuesta del estado de la operacion
*/
String API::setDeviceState(String deviceName, String state)
{
    IODevice *device = findDevice(deviceName);
    if (device == nullptr)
        return DEVICE_NOT_FOUND + deviceName;
    if ((*device).getType() != OUTPUT)
        return NON_OUTPUT_DEVICE_ERROR;
    state.toUpperCase();
    
    if (state == S_ON)
        digitalWrite((*device).getGPIO(), HIGH);
    else if (state == S_OFF)
        digitalWrite((*device).getGPIO(), LOW);
    else
        return NON_BOOLEAN_VALUE;

    return OK_RESPONSE;
}

/** Metodo para obtener el estado de un dispositivo
 * 
 * @param device_name nombre del dispositivo
 * @return respuesta de la operacion
*/
String API::getDeviceState(String deviceName)
{
    IODevice *device = findDevice(deviceName);
    if (device == nullptr)
        return DEVICE_NOT_FOUND + deviceName;

    bool value = digitalRead((*device).getGPIO());
    return (value) ? S_ON : S_OFF;
}