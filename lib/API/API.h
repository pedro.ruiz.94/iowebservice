#ifndef API_h
#define API_h

#include <Arduino.h>
#include <Deviceutils.h>
#include <lang.h>

class API
{
    IODevice *devices;
    int nDevices;

public:
    API(IODevice devices[], int nDevices);
    IODevice *findDevice(String deviceName);
    void initializeDevices();
    String setDeviceState(String deviceName, String state);
    String getDeviceState(String deviceName);
};

#endif