#include "API.hpp"
#include "ESP8266_Utils.hpp"
#include "unity.h"

#ifdef UNIT_TEST
void test_find_device(void)
{
    TEST_ASSERT_EQUAL({"door", INPUT, 14}, findDevice("door"))
    TEST_ASSERT_EQUAL({"lock", OUTPUT, 15}, findDevice("lock"))
    // TEST_ASSERT_EQUAL({NULL, INPUT, 14}, findDevice("nonExistent"))
}

int main(int argc, char **argv)
{
    UNITY_BEGIN();
    RUN_TEST(test_find_device);
    UNITY_END();

    return 0;
}
#endif