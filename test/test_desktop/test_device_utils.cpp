#include "unity.h"
#include "DeviceUtils.h"

#ifdef UNIT_TEST
void test_constructor(void)
{
    IODevice device("name", INPUT, 100);

    TEST_ASSERT_EQUAL("name", device.getName());
    TEST_ASSERT_EQUAL(INPUT, device.getType());
    TEST_ASSERT_EQUAL(100, device.getGPIO());
}

int main(int argc, char **argv)
{
    UNITY_BEGIN();
    RUN_TEST(test_constructor);
    UNITY_END();

    return 0;
}
#endif