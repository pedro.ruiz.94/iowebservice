#include <WebServer.h>
#include <unity.h>

#ifdef UNIT_TEST

void test_find_extra(void)
{
    String root = "test/root/";
    String request = root + "extra1/extra2";
    request.replace(root, "");

    TEST_ASSERT_EQUAL_STRING("extra1/extra2", request.c_str());

    String extra = findExtra(&request);
    TEST_ASSERT_EQUAL_STRING("extra1", extra.c_str());
    TEST_ASSERT_EQUAL_STRING("extra2", request.c_str());

    extra = findExtra(&request);
    TEST_ASSERT_EQUAL_STRING("extra2", extra.c_str());
    TEST_ASSERT_EQUAL_STRING("", request.c_str());

    request = "extra3";
    extra = findExtra(&request);
    TEST_ASSERT_EQUAL_STRING("extra3", extra.c_str());
    TEST_ASSERT_EQUAL_STRING("", request.c_str());
}
#endif