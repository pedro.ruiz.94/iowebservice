#include <DeviceUtils.h>
#include <unity.h>

#ifdef UNIT_TEST
void test_constructor(void)
{
    String name = "name";
    IODevice device(name, INPUT, 100);

    TEST_ASSERT(name == device.getName());
    TEST_ASSERT_EQUAL(INPUT, device.getType());
    TEST_ASSERT_EQUAL(100, device.getGPIO());

    String name2 = "name2";
    IODevice device2(name2, OUTPUT, 200);

    TEST_ASSERT(name2 == device2.getName());
    TEST_ASSERT_EQUAL(OUTPUT, device2.getType());
    TEST_ASSERT_EQUAL(200, device2.getGPIO());
}



void test_operator_equals(void)
{
    IODevice device("name", INPUT, 1);
    IODevice device1("name", INPUT, 1);
    IODevice device2("name1", INPUT, 1);
    IODevice device3("name", OUTPUT, 1);
    IODevice device4("name", INPUT, 2);

    TEST_ASSERT_TRUE(device == device1);
    TEST_ASSERT_FALSE(device == device2);
    TEST_ASSERT_FALSE(device == device3);
    TEST_ASSERT_FALSE(device == device4);
}

#endif