#include <unity.h>
#include "test_api.hpp"
#include "test_device_utils.hpp"
#include "test_web_server.hpp"

#ifdef UNIT_TEST
void setup()
{
    // NOTE!!! Wait for >2 secs
    // if board doesn't support software reset via Serial.DTR/RTS
    delay(2000);

    UNITY_BEGIN();
    // Test DeviceUtils
    RUN_TEST(test_constructor);
    RUN_TEST(test_operator_equals);
    // Test API
    RUN_TEST(test_find_device);
    RUN_TEST(test_device_state);
    // Test WebServer
    RUN_TEST(test_find_extra);
    UNITY_END();
}

void loop()
{
    digitalWrite(16, HIGH);
    delay(100);
    digitalWrite(16, LOW);
    delay(500);
}
#endif