#include <API.h>
#include <unity.h>

#ifdef UNIT_TEST

IODevice door("door", INPUT, 14);
IODevice lock("lock", OUTPUT, 15);
IODevice testDevices[] = {door, lock};
API testApi(testDevices, 2);

void setUp(void)
{
    testApi.initializeDevices();
}

void test_find_device(void)
{
    TEST_ASSERT_EQUAL(testApi.findDevice("null"), nullptr);

    IODevice *door = testApi.findDevice("door");
    TEST_ASSERT_TRUE((*door).getName() == "door");
    TEST_ASSERT_EQUAL(INPUT, (*door).getType());
    TEST_ASSERT_EQUAL(14, (*door).getGPIO());

    IODevice *lock = testApi.findDevice("lock");
    TEST_ASSERT_TRUE((*lock).getName() == "lock");
    TEST_ASSERT_EQUAL(OUTPUT, (*lock).getType());
    TEST_ASSERT_EQUAL(15, (*lock).getGPIO());
}

void test_device_state(void)
{
    String response = testApi.setDeviceState("door", "ON");
    TEST_ASSERT_EQUAL_STRING(NON_OUTPUT_DEVICE_ERROR, response.c_str());
    response = testApi.setDeviceState("inexistent", "ON");
    String expected = String(DEVICE_NOT_FOUND) + "inexistent";
    TEST_ASSERT_EQUAL_STRING(expected.c_str(), response.c_str());
    response = testApi.setDeviceState("lock", "unexpected");
    TEST_ASSERT_EQUAL_STRING(NON_BOOLEAN_VALUE, response.c_str());
    response = testApi.setDeviceState("lock", "ON");
    TEST_ASSERT_EQUAL_STRING(OK_RESPONSE, response.c_str());
    response = testApi.getDeviceState("lock");
    TEST_ASSERT_EQUAL_STRING(S_ON, response.c_str());
    response = testApi.setDeviceState("lock", "OFF");
    TEST_ASSERT_EQUAL_STRING(OK_RESPONSE, response.c_str());
    response = testApi.getDeviceState("lock");
    TEST_ASSERT_EQUAL_STRING(S_OFF, response.c_str());
}

#endif